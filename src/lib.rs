#![cfg_attr(test, no_std)]
#![feature(allocator_api, alloc_layout_extra, core_intrinsics, ptr_internals, raw_ref_op, raw_vec_internals)]

extern crate alloc;

use alloc::alloc::{AllocErr, AllocInit, AllocRef, Layout};
use core::ptr::{self, NonNull};

pub fn allocate_value<T, A: AllocRef>(value: T, mut allocator: A) -> Result<NonNull<T>, AllocErr> {
    let block = allocator.alloc(Layout::new::<T>(), AllocInit::Uninitialized)?;
    let pointer = block.ptr.cast::<T>();

    unsafe {
        ptr::write(pointer.as_ptr(), value);
    }

    Ok(pointer)
}

pub mod arc;
pub use self::arc::*;

pub mod boxed;
pub use self::boxed::*;

#[cfg(test)]
mod tests {
    use super::*;
    use alloc::alloc::Global;

    extern crate std;

    #[test]
    fn boxed() {
        let test = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        let boxed = Box::new(test, Global);

        assert_eq!(&*boxed, &test);

        let inner = Box::into_inner(boxed);

        assert_eq!(&inner, &test);
    }

    #[test]
    fn arc() {
        let mut test = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        let ref1 = Arc::new(test, Global);
        let ref2 = Arc::clone(&ref1);
        let mut ref3 = Arc::clone(&ref2);

        assert_eq!(Arc::strong_count(&ref2), 3);
        assert_eq!(&*ref2, &test);

        drop(ref2);

        assert_eq!(Arc::get_mut(&mut ref3), None);

        assert_eq!(Arc::strong_count(&ref1), 2);
        assert_eq!(&*ref3, &test);

        drop(ref1);

        assert_eq!(Arc::get_mut(&mut ref3), Some(&mut test));

        drop(ref3);
    }

    #[test]
    fn arc_threads() {
        use std::{sync::Mutex, vec::Vec};

        // Highly efficient parallel implementation of the multiplication
        // operator (x * y)

        let x = 10;
        let y = 20;

        let mut arc: Arc<Mutex<usize>> = Arc::new(Mutex::new(0), Global);

        let mut threads = Vec::with_capacity(x);

        for _ in 0..x {
            let arc_clone = Arc::clone(&arc);

            threads.push(std::thread::spawn(move || {
                for _ in 0..y {
                    *arc_clone.lock().unwrap() += 1;
                }
            }));
        }

        for thread in threads {
            thread.join().unwrap();
        }

        assert_eq!(Arc::strong_count(&arc), 1);
        assert_eq!(*arc.lock().unwrap(), 200);

        // Lock-free way of doing it
        assert_eq!(
            Arc::get_mut(&mut arc).map(|mutex| mutex.get_mut().unwrap()),
            Some(&mut 200)
        );
    }
}
